import pandas as pd
import networkx as nx
import ast

df = pd.read_csv('revised_comparison_BGB.csv') # or revised_comparison_StGB.csv
G = nx.Graph()

df['keywords'] = df['keywords'].astype(str)
df['keywords'] = df['keywords'].apply(ast.literal_eval)

for i, row in df.iterrows():

    p_row_list = []
    p_row_complete_title = []

    # iterating through the newly created columns and keyword_lists
    # and seperate paragraph numbers from paragraph titles
    for j in range(4, df.shape[1]):

        try:
            paragraph_string = df.iloc[i, j]
            paragraph_comparison_list = paragraph_string.split('\n')
            len_paragraph_list = len(paragraph_comparison_list)

            for k in range(0, len_paragraph_list):

                p = paragraph_comparison_list[k]

                p_title = p.split('§')[1]
                p_number = p_title.split()[0]
                p_row_list.append(p_number)

        except Exception as e:
            pass

    for l in range(0, len(p_row_list)):

        #### due to too many nodes only considering references with weights > 3 #####
        keyword_columns = [4, 5,
                           6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        for x in range(0, len(keyword_columns)):

            try:

                if p_row_list.count(p_row_list[l]) == keyword_columns[x]:

                    # add nodes and edges
                    if df.iloc[i, 0] not in G:
                        G.add_node(df.iloc[i, 0], label=df.iloc[i, 1])

                    if p_row_list[l] not in G:
                        G.add_node(p_row_list[l],
                                   label=df.iloc[p_row_list[l], 1])

                    if not G.has_edge(df.iloc[i, 0], p_row_list[l]):
                        G.add_edge(df.iloc[i, 0], p_row_list[l],
                                   weight=keyword_columns[x])

            except Exception as e:
                pass

nx.write_gexf(G, "BGB_weighted_weights>3.gexf") # or StGB_weighted_weights>3.gexf
