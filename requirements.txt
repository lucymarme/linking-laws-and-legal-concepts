networkx==2.2
pandas==0.24.2
requests==2.25.0
beautifulsoup4==4.9.3
yake==0.4.6
