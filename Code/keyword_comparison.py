import pandas as pd
import ast


df = pd.read_csv('scraping_revised_BGB.csv') # or scraping_revised_StGB.csv
df['keywords'] = df['keywords'].astype(str)
df['keywords'] = df['keywords'].apply(ast.literal_eval)


# general idea
# create 5 more columns for each of the keywords of one paragraph
# checking if each keyword individually exists more than once
# and storing related paragraph into keyword specific column

df['keyword_01_comparison'] = ""
df['keyword_02_comparison'] = ""
df['keyword_03_comparison'] = ""
df['keyword_04_comparison'] = ""
df['keyword_05_comparison'] = ""
df['keyword_06_comparison'] = ""
df['keyword_07_comparison'] = ""
df['keyword_08_comparison'] = ""
df['keyword_09_comparison'] = ""
df['keyword_10_comparison'] = ""
df['keyword_11_comparison'] = ""
df['keyword_12_comparison'] = ""
df['keyword_13_comparison'] = ""
df['keyword_14_comparison'] = ""
df['keyword_15_comparison'] = ""

# iterating through rows,
# storing the keyword_string and splitting it into the actual keywords

for i, row in df.iterrows():

    print(i)

    keyword_string = []
    keyword_string = df.iloc[i, 3]

    for x in range(len(keyword_string)):

        keyword_list = ""

        try:

            for k, row in df.iterrows():

                # checking if keyword exists in other row
                # if true storing associated paragraph_title in keyword_list
                if keyword_string[x] in df.iloc[k, 3]:

                    if k != i:
                        keyword_list += df.iloc[k, 1] + "\n"

            # inserting keyword_list with associated paragraph_titles into the right column
            column_index = x + 4
            df.iloc[i, column_index] = keyword_list

        except Exception as e:
            pass

#### writing new dataframe into the csv-file ####
df.to_csv('revised_comparison_BGB.csv', index=False) # or revised_comparison_StGB.csv
