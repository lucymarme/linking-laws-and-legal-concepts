import pandas as pd

# or paragraph_scraping_bgb.csv
df = pd.read_csv('paragraph_scraping_stgb.csv')

# creating list with keywords of every §
keyword_list = []

for k, row in df.iterrows():

    keyword_string = df.iloc[k, 3]

    for x in range(1, 16):

        try:
            keyword_split_bracket = keyword_string.split('(')[x]

            keyword_split_comma = keyword_split_bracket.split(',')[0]

            keyword_list.append(keyword_split_comma)

        except Exception as e:
            pass

# append keywords which appear in more than 10 percent of the §§
# and keywords which appear only once to word_freq[]

word_freq = []
count_row = df.shape[0]
x = count_row * 0.1

for w in range(0, len(keyword_list)):

    if keyword_list.count(keyword_list[w]) == 1:
        word_freq.append(keyword_list[w])

    if keyword_list.count(keyword_list[w]) > x:
        word_freq.append(keyword_list[w])

exception_list_general = ["'nummer'", "'satz'", "'abs'",
                          "'absatz'", "'weggefallen'", "'absatzes'", "'abs.'"]

# iterate through rows and filter keywords with word_freq[] and exception_list_general[]
for k, row in df.iterrows():

    keyword_string = df.iloc[k, 3]
    new_keyword_list = []

    for x in range(1, 16):

        try:
            keyword_split_bracket = keyword_string.split('(')[x]

            keyword_split_comma = keyword_split_bracket.split(',')[0]

            if keyword_split_comma not in word_freq and keyword_split_comma not in exception_list_general:
                new_keyword_list.append(keyword_split_comma)

        except Exception as e:
            pass

    df.iloc[k, 3] = new_keyword_list

# or scraping_revised_BGB.csv
df.to_csv('scraping_revised_StGB.csv', index=False)
