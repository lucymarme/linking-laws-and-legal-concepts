from bs4 import BeautifulSoup
import requests
PYTHONIOENCODING = "UTF-8"
import csv
import yake

class Scraping:

    # exception_chapters
    def __init__(self, name, exception01, exception02, exception03):

        self.name = name
        self.exception01 = exception01
        self.exception02 = exception02
        self.exception03 = exception03

    def scraping(self):

        source = requests.get(
            'https://www.gesetze-im-internet.de/' + self.name + '/').text

        soup = BeautifulSoup(source, 'lxml')

        table = soup.find('table')

        language = "de"
        max_ngram_size = 1
        deduplication_thresold = 0.9
        deduplication_algo = 'seqm'
        windowSize = 1

        with open('paragraph_scraping_' + self.name + '.csv', 'w') as paragraph_scraping_self:

            csv_writer = csv.writer(paragraph_scraping_self)
            csv_writer.writerow(
                ['paragraph_number', 'paragraph_title', 'paragraph_content', 'keywords'])

            for i in table.find_all('a', href=True):

                href = i['href']

                if self.exception01 not in href and self.exception02 not in href and self.exception03 not in href:

                    link = 'https://www.gesetze-im-internet.de/' + self.name + '/' + href
                    text_link = requests.get(link)
                    soup_paragraph = BeautifulSoup(text_link.text, 'lxml')

                    try:
                        #(BGB/StGB/StVO) is always appearing in the heading before the actual paragraph title
                        complete_header = soup_paragraph.h1.text
                        header = complete_header.split(')')[1]
                        paragraph_number = header.split()[1]

                        content_text = ""

                        for content in soup_paragraph.find_all('div', class_='jurAbsatz'):
                            content_text += content.text

                        try:
                            row_string = header + content_text

                            # amount of words in 'row_string' to see how many keywords should be listed
                            row_string_split = row_string.split()
                            amount_words = len(row_string_split)
                            print(amount_words)

                            x = amount_words * 0.2

                            if x < 5:
                                x = 5
                            if x > 15:
                                x = 15

                            numOfKeywords = x

                            kw_extractor = yake.KeywordExtractor(lan=language, n=max_ngram_size, dedupLim=deduplication_thresold,
                                                                 dedupFunc=deduplication_algo, windowsSize=windowSize, top=numOfKeywords, features=None)
                            keywords = kw_extractor.extract_keywords(
                                row_string)

                        except Exception as e:
                            pass

                        csv_writer.writerow(
                            [paragraph_number, header, content_text, keywords])

                    except Exception as e:
                        pass


#### BGB ####
# bgb_scraping = Scraping('bgb', 'BJNR001950896',
#                         'inhalts_bersicht', '-1')
# bgb_scraping.scraping()


#### StGB ####
# stgb_scraping = Scraping('stgb', 'BJNR001270871',
#                          'inhalts_bersicht', '-1')
# stgb_scraping.scraping()

#### StVO ####
# stvo_scraping = Scraping('stvo_2013', 'BJNR036710013',
#                          'anlage_', 'schlussformel')
# stvo_scraping.scraping()
