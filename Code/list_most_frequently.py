import pandas as pd
import operator

df = pd.read_csv('paragraph_scraping_stgb.csv') # or paragraph_scraping_bgb.csv

# seperate keywords and append them to keyword_list[]

keyword_list = []

for k, row in df.iterrows():

    keyword_string = df.iloc[k, 3]

    for x in range(1, 16):

        try:
            keyword_split_bracket = keyword_string.split('(')[x]

            keyword_split_comma = keyword_split_bracket.split(',')[0]

            keyword_list.append(keyword_split_comma)

        except Exception as e:
            pass

# determine frequency of each keyword

word_freq = []

for w in keyword_list:
    word_freq.append(keyword_list.count(w))

z = zip(keyword_list, word_freq)

s = sorted(z, key=operator.itemgetter(1))

print(str(s))
