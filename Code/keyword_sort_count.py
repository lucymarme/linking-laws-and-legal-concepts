import pandas as pd
import ast


df = pd.read_csv('scraping_revised_StGB.csv')  # or scraping_revised_BGB.csv
keyword_list = []
keyword_list_filtered = []
amount_words = []

df['keywords'] = df['keywords'].astype(str)
df['keywords'] = df['keywords'].apply(ast.literal_eval)

for i, row in df.iterrows():

    keyword_list_temp = []
    keyword_list_temp = df.iloc[i, 3]

    for x in range(0, len(keyword_list_temp)):
        keyword_list.append(keyword_list_temp[x])

    try:
        amount_words_string = ""
        amount_words_string = df.iloc[i, 2]
        amount_words_temp = amount_words_string.split()

        for x in range(0, len(amount_words_temp)):
            amount_words.append(amount_words_temp[x])

    except Exception as e:
        pass

#### total number of keywords ####
print("Anzahl der gesamten Keywords:", len(keyword_list))

#### total number of words ####
print("Gesamtanzahl Wörter:", len(amount_words))

#### filter duplications ####
for l in range(0, len(keyword_list)):

    if keyword_list[l] not in keyword_list_filtered:
        keyword_list_filtered.append(keyword_list[l])

print("Anzahl Keywords ohne Doppelungen:", len(keyword_list_filtered))

#### sort alphabetically ####
keyword_list_sorted = sorted(keyword_list_filtered, key=str.lower)

print("Keywords alphabetisch sortiert:" + "\n", "\n".join(keyword_list_sorted))
